# Python Embedded in R

The following is only intended for testing purposes for windows only.


If you get an "api-ms-win-crt-runtime-l1-1.0.dll is missing" error it means you should install [Visual C++ Redistributable für Visual Studio 2015](https://download.microsoft.com/download/C/E/5/CE514EAE-78A8-4381-86E8-29108D78DBD4/VC_redist.x64.exe)


```r
library(PythonEmbeddedInR)
ls("package:PythonEmbeddedInR")
```


Downloads Python into your package directory.
```r
get_python()
```

Connects to Python and executes `print("Hello R!")` if verbose is true the Python startup message get's printed to the screen.
```r
hello_R(verbose=TRUE)
```


```
q("no")
library(PythonEmbeddedInR)
hello_R(verbose=FALSE)
```